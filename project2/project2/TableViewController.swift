//
//  SecondViewController.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData
import MapKit

let LOCATION_SELECTED = NSNotification.Name("LOCATION_SELECTED")
var selectedLocation: Location!
var comingFromList = false
class TableViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource {
	var frc: NSFetchedResultsController<NSFetchRequestResult>!
	let CELL_ID = "cell"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		initFRC()
	}

	@IBAction func goingToAddAction(_ sender: Any) { comingFromList = true }
	@IBAction func refreshBtnAction(_ sender: Any) {
		if (MapViewController.isConnected())
		{
			CDManager.updateFromServer()
		} else
		{
			MapViewController.createAndShowAlert("You're Offline",
												 "Looks like you're offline. New pins can't be fetched but you can continue to add your own.",
												 "OK", self)
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// Set up the cell
		let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath)
		guard let object = self.frc?.object(at: indexPath) else {
			fatalError("Attempt to configure cell without a managed object")
		}
		
		let location = object as! Location
		cell.textLabel?.text = location.locationName
		cell.detailTextLabel?.text = location.locationDescription
		return cell
	}
	
	 func numberOfSections(in tableView: UITableView) -> Int {
		return frc.sections!.count
	}
	
	 func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedLocation = frc.object(at: indexPath) as! Location
		performSegue(withIdentifier: "toMapFromTable", sender: self)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "toMapFromTable") { userSelectionHappend = true }
	}
	
	 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = frc.sections else {
			fatalError("[TableViewController]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
	}
	
	// MARK: - initialize the FetchResultsController
	func initFRC() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: LOCATION_ENTITY)
		let sortBy = NSSortDescriptor(key: "locationID", ascending: true)
		request.sortDescriptors = [sortBy]
		let moc = CDManager.context()
		frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
		frc.delegate = self
		do {
			try frc.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
	}
}
