//
//  CDManger.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import SwiftyJSON

let LOCATION_ENTITY = "Location"
let LOCATION_ADDED = NSNotification.Name("LOCATION_ADDED")
private let requestString = "https://s3-us-west-2.amazonaws.com/electronic-armory/buildings.json"
/**
`CDManager` is the main class use to interface with Core Data
and the various data model entities. It has helper functions
to fetch various model entites and the current context.

- Author: Prince Kannah
*/
class CDManager {
	
	private init(){ /** can't be called directly **/ }
	
	/**
	Retrieve the managed object context from the container.
	- Returns: the manaaged context.
	*/
	class func context() -> NSManagedObjectContext {
		return persistentContainer.viewContext
	}
	
	class func fetchLocations() {
		Alamofire.request(requestString).responseJSON
			{ response in
			if let responseJSON = response.result.value
			{
				let jsonArray = responseJSON as! NSArray
				for object in jsonArray
				{
					let json				= JSON(object)
					let location			= Location(context: context())
					location.locationID 	= json["id"].stringValue
					location.locationName 	= json["name"].stringValue
					location.locationDescription = json["description"].stringValue
					location.userCreated	= false
					
					let coordinates 		= json["location"].dictionaryValue
					location.latitude 	= (coordinates["latitude"]?.doubleValue)!
					location.longitude 	= (coordinates["longitude"]?.doubleValue)!
					NotificationCenter.default.post(name: LOCATION_ADDED, object: location)
				}
				saveContext()
			}
		}
	}
	
	class func updateFromServer() {
		Alamofire.request(requestString).responseJSON
			{ response in
				if let responseJSON = response.result.value
				{
					let jsonArray = responseJSON as! NSArray
					for object in jsonArray
					{
						let json				= JSON(object)
						let id 				= json["id"].stringValue
						
						let request: NSFetchRequest<Location> = Location.fetchRequest()
						let predicate = NSPredicate(format: "locationID = %@", "\(id)")
						request.predicate = predicate
						
						do {
							let results = try CDManager.context().fetch(request)
							
							if (results.count == 0)
							{
								let location			= Location(context: context())
								location.locationID 	= id
								location.locationName 	= json["name"].stringValue
								location.locationDescription = json["description"].stringValue
								location.userCreated	= false
								
								let coordinates 		= json["location"].dictionaryValue
								location.latitude 	= (coordinates["latitude"]?.doubleValue)!
								location.longitude 	= (coordinates["longitude"]?.doubleValue)!
								NotificationCenter.default.post(name: LOCATION_ADDED, object: location)
							}
						} catch {
							fatalError("[CDMangaer]Unable to fetch location")
						}
						
					}
					saveContext()
				}
		}
	}
	/**
	Fetch all location from Core Data
	
	- Returns: all saved location
	*/
	class func fetchLocation () -> [Location]? {
		do {
			let request: NSFetchRequest<Location> = Location.fetchRequest()
			let results = try CDManager.context().fetch(request)
			return results
		} catch {
			fatalError("[CDManager]Unable to fetch Location from Core Data: \(error)")
		}
		return nil
	}
	
	// MARK: - Core Data stack
	
	static var persistentContainer: NSPersistentContainer = {
		/*
		The persistent container for the application. This implementation
		creates and returns a container, having loaded the store for the
		application to it. This property is optional since there are legitimate
		error conditions that could cause the creation of the store to fail.
		*/
		let container = NSPersistentContainer(name: "Location")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate.
				// You should not use this function in a shipping application,
				// although it may be useful during development.
				
				/*
				Typical reasons for an error here include:
				* The parent directory does not exist, cannot be created, or disallows writing.
				* The persistent store is not accessible, due to permissions or data protection when the device is locked.
				* The device is out of space.
				* The store could not be migrated to the current model version.
				Check the error message to determine what the actual problem was.
				*/
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()
	
	// MARK: - Core Data Saving support
	
	class func saveContext() {
		let context = persistentContainer.viewContext
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				// Replace this implementation with code to handle the error appropriately.
				// fatalError() causes the application to generate a crash log and terminate.
				// You should not use this function in a shipping application,
				// although it may be useful during development.
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}
}
