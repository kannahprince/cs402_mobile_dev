//
//  RoundedUIButton.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedUIButton: UIButton {

	@IBInspectable
	var cornerRadius: CGFloat = 0 {
		didSet {
			self.layer.cornerRadius = cornerRadius
		}
	}
	
	@IBInspectable
	var borderWidth: CGFloat = 0 {
		didSet {
			self.layer.borderWidth = borderWidth
		}
	}
	
	@IBInspectable
	var borderColor: UIColor = .clear {
		didSet {
			self.layer.borderColor = borderColor.cgColor
		}
	}
}
