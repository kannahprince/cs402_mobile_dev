//
//  AddLocationViewController.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class AddLocationViewController: UIViewController {
	@IBOutlet weak var saveBtn: UIButton!
	@IBOutlet weak var locationNameTextField: UITextField!
	@IBOutlet weak var locationDescriptionTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		LocationServices.startGPS()
	}
	
	// take user back to the list view instead of the initial
	// map view
	@IBAction func backToListAction(_ sender: Any) {
		if (comingFromList) {
			comingFromList = false
			performSegue(withIdentifier: "backToListFromAdd", sender: self)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "backToListFromAdd") {
			let dest = segue.destination as! UITabBarController
			dest.selectedIndex = 1
		}
	}
	
	@IBAction func saveBtnAction(_ sender: Any) {
		
		var locationName = ""
		let locationText = locationNameTextField.text
		if (!(locationText?.isEmpty)!) {
			locationName = (locationText?.trimmingCharacters(in: .whitespaces))!
		} else {
			MapViewController.createAndShowAlert("Empty location name", "Please provide a name for this pin.", "OK", self)
		}
		
		var locationDescription = ""
		let descriptionText = locationDescriptionTextField.text
		if (!(descriptionText?.isEmpty)!) {
			locationDescription = (descriptionText?.trimmingCharacters(in: .whitespaces))!
		} else {
			MapViewController.createAndShowAlert("No description", "Please provide a description for this pin.", "OK", self)
		}
		
		var hashString = ""
		if (!locationDescription.isEmpty && !locationName.isEmpty) {
			hashString = locationDescription + locationName
		}
		
		let newLocation = Location(context: CDManager.context())
		newLocation.locationID = "\(hashString.hashValue)"
		newLocation.locationName = locationName
		newLocation.locationDescription = locationDescription
		newLocation.userCreated = true
		newLocation.latitude = LocationServices.deviceLocation().latitude
		newLocation.longitude = LocationServices.deviceLocation().longitude
		
		// jump to the newly created pin on the map
		selectedLocation = newLocation
		userSelectionHappend = true
		LocationServices.stopGPS()
		CDManager.saveContext()
		performSegue(withIdentifier: "toMapFromAddAfterSaving", sender: self)
	}

}
