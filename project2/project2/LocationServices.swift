//
//  LocationServices.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreLocation

class LocationServices: NSObject, CLLocationManagerDelegate {
	private static let locationManager = CLLocationManager()
	private static let delegate:LocationServices = LocationServices()
	
	class func startGPS() {
		locationManager.delegate = delegate
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
	}
	
	//    MARK: - stop GPS
	class func stopGPS(){
		locationManager.stopUpdatingLocation()
	}
	//    MARK: - get user location
	class func deviceLocation() -> CLLocationCoordinate2D {
		return (locationManager.location?.coordinate)!
	}
	
//	class func initLocation()
//	{
//		switch CLLocationManager.authorizationStatus()
//		{
//		case .notDetermined:
//			// Request when-in-use authorization initially
//			locationManager.requestWhenInUseAuthorization()
//		case .restricted, .denied: break
//		// Disable location features; show input filed for state initial
//		case .authorizedWhenInUse:
//			// Enable basic location features;
//			// Do a reverse geocoding to get user's state abreviation
//		case .authorizedAlways: break
//			// Enable any of your app's location features; set up geo fence to
//			// show current location's rep
//			//enableMyAlwaysFeatures()
//		}
//	}
	
//	func locationManager(_ manager: CLLocationManager,
//						 didChangeAuthorization status: CLAuthorizationStatus) {
//		switch status {
//		case .restricted, .denied:
//			// Disable your app's location features
//			//disableMyLocationBasedFeatures()
//			break
//		case .authorizedWhenInUse:
//			// Enable only your app's when-in-use features.
//			//enableMyWhenInUseFeatures()
//			break
//		case .authorizedAlways:
//			// Enable any of your app's location services.
//			// enableMyAlwaysFeatures()
//			break
//		case .notDetermined:
//			break
//		}
//	}
}
