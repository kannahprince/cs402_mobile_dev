//
//  FirstViewController.swift
//  project2
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Alamofire
import UIKit
import MapKit

var userSelectionHappend = false

class MapViewController: UIViewController {
	let REGION_RADIUS: CLLocationDistance = 1000
	let initialZoom = 2.6
	/* location to zoom in on when app starts. These are
	the coordinats for Julia Davis Park in Boise, ID */
	let INIT_LOCATION:CLLocation = CLLocation(latitude: 43.607649, longitude: -116.201534)

	@IBOutlet weak var mapView: MKMapView!
	@IBAction func refreshBtnAction(_ sender: Any) {
		if (MapViewController.isConnected())
		{
			CDManager.updateFromServer()
		} else
		{
			MapViewController.createAndShowAlert(" You're offline",
												 "Looks like you're offline. New pins can't be fetched but you can continue to add your own.",
												 "OK", self)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		centerMapOnLocation(location: INIT_LOCATION, zoom: initialZoom)
		
		if (userSelectionHappend) {
			let zoomInOn = CLLocation(latitude: selectedLocation.latitude, longitude: selectedLocation.longitude)
			centerMapOnLocation(location: zoomInOn, zoom: 0.5)
			userSelectionHappend = false
		}
		
		// listen for any new location added to Core Data
		NotificationCenter.default.addObserver(forName: LOCATION_ADDED, object: nil, queue: nil) { notification in
			let pin = notification.object as! Location
			
			let annotation = MKPointAnnotation()
			let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(pin.latitude, pin.longitude)
			annotation.title = pin.locationName
			annotation.subtitle = pin.locationDescription
			annotation.coordinate = coordinate
			self.mapView.addAnnotation(annotation)
		}

		// if we've already launch, there are locations in Core Data
		// load those
		if (UserDefaults.standard.bool(forKey: "firstLaunch")) {
			for locaiton in CDManager.fetchLocation()!
			{
				let annotation = MKPointAnnotation()
				let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(locaiton.latitude, locaiton.longitude)
				annotation.title = locaiton.locationName
				annotation.subtitle = locaiton.locationDescription
				annotation.coordinate = coordinate
				self.mapView.addAnnotation(annotation)
			}
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if (!MapViewController.isConnected()) {
			// let's show this alert only once
			if (!UserDefaults.standard.bool(forKey: "offlineAlertShown")) {
				MapViewController.createAndShowAlert(" You're Offline",
													 "Looks like you're offline. New pins can't be fetched but you can continue to add your own.",
													 "OK", self)
				UserDefaults.standard.set(true, forKey: "offlineAlertShown")
			}
		} else { UserDefaults.standard.set(false, forKey: "offlineAlertShown") }
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	class func isConnected() -> Bool {
		return NetworkReachabilityManager()!.isReachable
	}
	
	class func createAndShowAlert(_ alertTitle: String, _ alertMessage: String, _ actionTitle: String, _ view: UIViewController) {
		// create the alert
		let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
		
		// add an action
		alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default) { _ in
			// Hide the alert
			alert.dismiss(animated: true, completion: nil)
		})
		
		// show the alert
		view.present(alert, animated: true, completion: nil)
	}
	
	/**
	Center the map view on the specified location
	
	- Parameter location: the location to center the map on
	*/
	func centerMapOnLocation(location: CLLocation, zoom: Double) {
		let region = MKCoordinateRegionMakeWithDistance(location.coordinate, REGION_RADIUS * zoom, REGION_RADIUS * zoom)
		mapView.setRegion(region, animated: true)
	}
}
