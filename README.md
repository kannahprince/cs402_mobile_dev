# CS402 - Mobile Development (iOS)
This repo contains the source code for my CS402 mobile development. Also includes homework.

All projects use [CocoaPods](https://cocoapods.org) for dependency management. If you have that install then clone the repo, run `pod install` in the root of the project. Once that is complete open the workspace (`.xcworkspace`) file in Xcode.
