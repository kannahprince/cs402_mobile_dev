//
//  PinBuilding.swift
//  project1
//
//  Created by Prince Kannah on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.
//

import MapKit

class BuildingPin: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var id:Int
    
    init(title:String, subtitle:String, coordinate:CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.id = -42
    }
}
