//
//  ViewController.swift
//  project1
//
//  Created by Prince Kannah on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.

import UIKit
import MapKit

class ViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    let REGION_RADIUS: CLLocationDistance = 1000
    /* location to zoom in on when app starts. These are
     the coordinats for Boise, ID */
    let INIT_LOCATION:CLLocation = CLLocation(latitude: 43.61871, longitude: -116.214607)
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  REGION_RADIUS * 3.5, REGION_RADIUS * 3.5)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // start up app zoomed in in Boise
        centerMapOnLocation(location: INIT_LOCATION)
        // listen for notification for each new building that
        // gets loaded from the JSON
        NotificationCenter.default.addObserver(forName: BuildingController.BUILDING_ADDED_NOTIFICATION, object: nil, queue: nil){ notification in
            let newPin = notification.object as! BuildingPin
            self.mapView.addAnnotation(newPin)
        }
        
        let buildings = BuildingController.shareBuildings()
        for(_, object) in buildings.enumerated(){
            let buildingPin:BuildingPin = object as! BuildingPin
            mapView.addAnnotation(buildingPin)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
