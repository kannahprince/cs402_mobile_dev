//
//  BuildingController.swift
//  project1
//
//  Created by Prince Kannah on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

class BuildingController: NSArray {
    static var buildings:NSArray = NSArray()
    // location of the Building JSON file
    static let JSON_URL:String = "https://s3-us-west-2.amazonaws.com/electronic-armory/buildings.json"
    public static let BUILDING_ADDED_NOTIFICATION = NSNotification.Name("Building Added")
    // return a copy of the list of buildings
    class func shareBuildings() -> NSArray{
        let retBuilding:NSArray = NSArray()
        
        for building in buildings {
            retBuilding.adding(building)
        }
        return retBuilding
    }
    
    // add the specified building to the list
    class func addBuilding(building:BuildingPin){
        buildings.adding(building)
        NotificationCenter.default.post(name: BUILDING_ADDED_NOTIFICATION, object: building)
    }
    
    // while the app is starting up, load the JSON file using AlFire. This method is called
    // in AppDelegate.swift
    class func loadBuildings(){
        //Make a HTTPS request using Alamofire
        // and dig through the response
        Alamofire.request(JSON_URL).responseJSON{ response in
            // cast the body of the response as to an Array to index into
            if let buildingJSON:NSArray = response.result.value as? NSArray{
                
                //Loop through entire JSON, storing each object
                // into buildingObject, also JSON
                for buildingObject in buildingJSON {
                    var currentBuilding:Dictionary<String, Any> = buildingObject as! Dictionary
                    // pull out current building info to store in int PinBuilding object
                    let name:String = currentBuilding["name"] as! String
                    let id:Int = currentBuilding["id"] as! Int
                    let description = currentBuilding["description"] as! String
                    var locationDict:Dictionary = currentBuilding["location"] as! Dictionary<String, Double>
                    let latitude:Double = locationDict["latitude"]!
                    let longitude:Double = locationDict["longitude"]!
                    
                    // create CLL coordinate
                    let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
                    
                    //create building Pin
                    let currentBuildingPin:BuildingPin = BuildingPin(title: name, subtitle: description, coordinate: location)
                    currentBuildingPin.id = id
                    
                    // add pins to list
                    addBuilding(building: currentBuildingPin)
                }
            }
        }
    }
}
