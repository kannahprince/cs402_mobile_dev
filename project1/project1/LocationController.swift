//
//  LocationController.swift
//  project1
//
//  Created by Labuser on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.
//

import Foundation
import CoreLocation

class LocationController: NSObject,CLLocationManagerDelegate {
    static public var currentLocation:CLLocation? = nil
    static let locationManager:CLLocationManager = CLLocationManager()
    static let sharedLocationController:LocationController = LocationController()
    
    class func startGPS(){
        //Ask permission to use GPS
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = sharedLocationController
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    class func stopGPS(){
        locationManager.stopUpdatingLocation()
    }
    
    class func deviceLocation() -> CLLocationCoordinate2D{
        return (locationManager.location?.coordinate)!
    }
    
}
