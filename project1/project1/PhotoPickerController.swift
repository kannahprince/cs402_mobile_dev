//
//  PhotoPickerController.swift
//  project1
//
//  Created by Prince Kannah on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.
//

import UIKit

class PhotoPickerController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var didShow:Bool = false
    let imageView: UIImageView! = nil
    
    override func viewDidAppear(_ animated: Bool) {
        if( !didShow ){
            didShow = true
            let imagePicker:UIImagePickerController = UIImagePickerController()
            
            #if TARGET_IPHONE_SIMULATOR // This is backwards
                imagePicker.sourceType = .camera
            #else
                imagePicker.sourceType = .photoLibrary
            #endif
            
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            
            present(imagePicker, animated: animated, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // dismiss the image picker then the parent view behind it
        picker.dismiss(animated: true, completion:nil)
        self.dismiss(animated: true, completion: nil)
    }
}
