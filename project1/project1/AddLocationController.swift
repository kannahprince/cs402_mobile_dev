//
//  AddLocationController.swift
//  project1
//
//  Created by Prinece Kannah on 9/28/17.
//  Copyright © 2017 Prince Kannah. All rights reserved.
//

import UIKit
import CoreLocation

class AddLocationController: UIViewController {
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // dismiss the addLocation view when the cancel button is pushed
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // action performed when the save button is pushed
    @IBAction func saveButtonAction(_ sender: Any) {
        let nameString:String = nameTextField.text!
                let description:String = descriptionTextField.text!
        
                let newLocation:BuildingPin = BuildingPin(title: nameString, subtitle: description, coordinate: LocationController.deviceLocation())
                BuildingController.addBuilding(building: newLocation)
                self.dismiss(animated: true, completion: nil)
            }
    }
