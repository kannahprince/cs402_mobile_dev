//
//  BackTableViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class BackTableViewController: UITableViewController {
    let REP_CELL_IDENTIFIER:String = "reps"
    let DIRECTORY_CELL_IDENTIFIER:String = "directory"
    let SETTINGS_CELL_IDENTIFIER:String = "settings"
    let ABOUT_CELL_IDENTIFIER:String = "about"
    let ADMIN_CELL_IDENTIFIER:String = "admin"
    let HOME_CELL_IDENTIFIER:String = "home"
    
    var backMenu = [String]()
    var backMenuID = [String]()
    
    override func viewDidLoad() {
        // do stuff
        backMenu = ["Home", "My representatives", "Directory", "Adminstration", "Settings", "About"]
        
        backMenuID = [HOME_CELL_IDENTIFIER, REP_CELL_IDENTIFIER, DIRECTORY_CELL_IDENTIFIER, ADMIN_CELL_IDENTIFIER, SETTINGS_CELL_IDENTIFIER, ABOUT_CELL_IDENTIFIER]
        
        // Add footer to hide empty cells
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        // to more stuff
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return backMenu.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: backMenuID[indexPath.row], for: indexPath)
        cell.textLabel?.text = backMenu[indexPath.row]
        return cell
    }
}
