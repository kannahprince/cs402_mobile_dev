//
//  BackTableViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MyRepViewController: UITableViewController {
    let REP_CELL_IDENTIFIER:String = "rep"
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    // There is the Senate & House of Representatives
    let NUMBER_OF_SECTIONS = 2
    
    // unlike Senators, representatives are based on population
    // so it's possible for a state to have a single rep
    let repType = ["Representatives", "Senators", "Representative"]
    @IBOutlet var myRepsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // THIS IS IMPORTANT -> DO NOT DELETE OR MOVE
        initializeFetchedResultsController()
        ///////////////////////////////////////////

        // Add footer to hide empty cells
        tableView.tableFooterView = UIView()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override func didReceiveMemoryWarning() {
        // to more stuff
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // TODO: return Representative if there is only one
        return repType[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: REP_CELL_IDENTIFIER, for: indexPath) as! MyRepCellView
        
        // Get object from CoreData
        guard let object = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        // unwrap the object and update cell
        let rep = object as! CongressPerson
        cell.firstNameLabel.text = rep.firstName
        cell.lastNameLabel.text  = rep.lastName
        cell.partyLabel.text     = rep.party
        cell.repImageInCell?.image = UIImage(named: "first")
        return cell
    }
    
    func initializeFetchedResultsController() {
        let request = DataController.createFetchRequest(nameOfEntity: DataController.CONG_PERSON_ENTITY)
         let chamberSort = NSSortDescriptor(key: "department.name", ascending: true)
        let lastNameSort = NSSortDescriptor(key: "firstName", ascending: true)
        request.sortDescriptors = [chamberSort, lastNameSort]
        
        let moc = DataController.getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "department.name", cacheName: nil)
        fetchedResultsController.delegate = self as? NSFetchedResultsControllerDelegate
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

class MyRepCellView: UITableViewCell {
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var partyLabel: UILabel!
    @IBOutlet weak var repImageInCell: UIImageView!
}

