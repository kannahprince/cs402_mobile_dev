//
//  TabBarController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        // set up sliding to the left to bring up the hidden menu (Ala Twitter on Andriod)
        // Setting it up in the TabBar controller allows this function on any section
        // of the tab bar view
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override func didReceiveMemoryWarning() {
        // do more stuff
    }
}
