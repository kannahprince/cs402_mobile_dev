//
//  ViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // do other stuff
        // set up sliding to the right to reveal hidden menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // do more stuff
    }
}

