//
//  DataFetcher.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//  All API docs can be found here: https://projects.propublica.org/api-docs/congress-api/members/#get-current-members-by-statedistrict
//

import Foundation
import CoreData
import Alamofire

private let API_KEY              = "rm8AfuIG7M7p0W2bLSAw6gFpvooWjiw6iNSdDLqQ"
public  let CURRENT_CONGRESS     = 115
public  let CURRENT_API_VERSION  = "v1"
public  let SENATE_ENDPOINT      = "senate"
public  let HOUSE_ENDPOINT       = "house"
private let headers: HTTPHeaders    = [ "X-API-Key": API_KEY ]
private let NULL_STRING = "<null>"

class DataFetcher {
    
    init() { /* NOTHING TO DO...YET */ }
    
    class func makeRequest(){
        requestAllMemberFor(chamber: SENATE_ENDPOINT)
    }
    
    class func requestSenatorsFromState(state:String){
        let requestString = craftRequestStringForSenate(chamber: SENATE_ENDPOINT, state: state)
        
        Alamofire.request(requestString, headers: headers).responseJSON { response in
            // sure we get back valid data before doing anything
            if let json = response.result.value {
                // unwrap the returned JSON into a dict
                let parseableJSON = json as! Dictionary<String, Any>
                _ = parseableJSON["status"]!
                let queryResults = parseableJSON["results"] as! NSArray
                for item in queryResults {
                   // TODO
                    print(item)
                }
            }else{
                fatalError("The Pro Publica Inc. servers are currently unreachable")
            }
        }
    }
    
    class func requestRepresentativesFromState(state:String){
        let requestString = craftRequestStringForSenate(chamber: SENATE_ENDPOINT, state: state)
        
        Alamofire.request(requestString, headers: headers).responseJSON { response in
            // sure we get back valid data before doing anything
            if let json = response.result.value {
                // unwrap the returned JSON into a dict
                let parseableJSON = json as! Dictionary<String, Any>
                _ = parseableJSON["status"]!
                let queryResults = parseableJSON["results"] as! NSArray
                for item in queryResults {
                    // TODO
                    print(item)
                }
            }else{
                fatalError("The Pro Publica Inc. servers are currently unreachable")
            }
        }
    }
    
    class func requestAllMemberFor(chamber:String, congress:Int? = CURRENT_CONGRESS) {
        let requestString = craftRequestStringForAllMembers(endpoint: chamber, chamber: chamber, congress: congress)
        var i = 0
        Alamofire.request(requestString, headers: headers).responseJSON { response in
            // sure we get back valid data before doing anything
            if let json = response.result.value {
                
                // unwrap the returned JSON into a dict
                let fetchResult:Dictionary = json as! Dictionary<String, Any>
                let queryResults = fetchResult["results"] as! NSArray
                let itemDict:Dictionary = queryResults[0] as! Dictionary<String, Any>
                _ = itemDict["chamber"]
                
                let members = itemDict["members"] as! NSArray
                
                for senator in members{
                    let details:Dictionary = senator as! Dictionary<String, Any>
                    
//                    for i in 0...50 {
                        let newData = NSEntityDescription.insertNewObject(forEntityName: DataController.SENATOR_ENTITY, into: DataController.getContext()) as! Senator
                        let house = NSEntityDescription.insertNewObject(forEntityName: DataController.DEPARTMENT_ENTITY, into: DataController.getContext()) as! Department
                        
                        if((i % 2) == 0){
                            house.name = "Senator"
                        }
                        else{
                            house.name = "Representative"
                        }
                        newData.firstName = details["first_name"] as? String
                        newData.lastName = details["last_name"] as? String
                        
                        let middle = details["middle_name"] as? String
                        if (middle != NULL_STRING){
                            newData.middleInitial = middle
                        }
                        newData.stateAbrev = details["state"] as? String
                        house.addToMembers(newData)
//                    }
//                    print(details)
                    i += 1
                }

            }else{
                fatalError("The Pro Publica Inc. servers are currently unreachable")
            }
        }
        DataController.saveContext()
    }
    
    class func craftRequestStringForMember(memberID:String) -> String {
        var uri = "https://api.propublica.org/congress/"
        uri += "\(CURRENT_API_VERSION)/members/\(memberID).json"
        return uri
    }
    
    class func craftRequestStringForAllMembers(endpoint:String, chamber:String, congress:Int? = CURRENT_CONGRESS) -> String{
        var str = "https://api.propublica.org/congress/"
        str += "\(CURRENT_API_VERSION)/\(congress!)/\(chamber.lowercased())/members.json"
        return str
    }
    
    class func craftRequestStringForSenate(chamber:String, state:String) -> String {
        var uri = "https://api.propublica.org/congress/"
        uri += "\(CURRENT_API_VERSION)/members/\(chamber.lowercased())/\(state.uppercased())/current.json"
        return uri
    }
    
    private func craftRequestStringForHouse(chamber:String, state:String, district:Int) -> String {
        var uri = "https://api.propublica.org/congress/"
        uri += "\(CURRENT_API_VERSION)/members/\(chamber.lowercased())/\(state.uppercased())/\(district)/current.json"
        return uri
    }
}
