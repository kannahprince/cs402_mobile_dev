//
//  DatabaseController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/13/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import CoreData

class DataController {
    
    // This entity is use to sort table into sections
    static let DEPARTMENT_ENTITY = "Department"
    static let SENATOR_ENTITY = "Senator"
    static let COMEITTE_ENTITY = "Committee"
    static let CONG_PERSON_ENTITY = "CongressPerson"
    static let REP_ENTITY = "Representative"
    
    private init(){ /** can't be called directly **/ }
    
    class func getContext() -> NSManagedObjectContext {
        return DataController.persistentContainer.viewContext
    }
    
    
    class func createFetchRequest(nameOfEntity:String) -> NSFetchRequest<NSFetchRequestResult>{
        switch nameOfEntity {
        case DEPARTMENT_ENTITY:
            return Department.fetchRequest()
        case COMEITTE_ENTITY:
            return Committee.fetchRequest()
        case CONG_PERSON_ENTITY:
            return CongressPerson.fetchRequest()
        case SENATOR_ENTITY:
            return Senator.fetchRequest()
        case REP_ENTITY:
            return Representative.fetchRequest()
        default:
            return NSFetchRequest<NSFetchRequestResult>(entityName: nameOfEntity)
        }
    }
    
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Congressional_Directory")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("WE ARE SAVING SHIT")
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
