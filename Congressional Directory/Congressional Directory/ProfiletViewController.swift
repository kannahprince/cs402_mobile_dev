//
//  FirstViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/12/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "hasLaunched")
        // Do any additional setup after loading the view, typically from a nib.
//        let repsFetch = DataController.createFetchRequest(nameOfEntity: DataController.CONG_PERSON_ENTITY)
//        
//        do {
//            let fetchedReps = try DataController.getContext().fetch(repsFetch) as! [CongressPerson]
//            
//            if (fetchedReps.count > 0) {
//                for rep in fetchedReps {
//                    print(rep)
//                }
//            }
//        } catch {
//            fatalError("Failed to fetch employees: \(error)")
//        }
        
        DataFetcher.makeRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

